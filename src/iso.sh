#!/bin/sh

printf '%b\n' "\n\033[1m"WINDOWS_INIT:"\033[0m"" Executing extract_iso.sh"
src/extract_iso.sh
EXTRACT="win/"

#printf '%b\n' "\n\033[1m"WINDOWS_INIT:"\033[0m"" Executing bootsect.sh"
#./src/bootsect.sh
#BOOT="bootsect.bin"

# add answerfile
cp data/autounattend.xml ${EXTRACT}

# data for mkisofs
BOOT="boot/etfsboot.com"
EFI="efi/boot/bootx64.efi"
LOAD_SEG="$(dumpet -i Win*.iso \
    | grep "Media load segment: " \
    | cut -d ':' -f2 \
    | cut -d ' ' -f2)"
LOAD_SIZE="$(dumpet -i Win*.iso \
    | grep "Load Sectors: " \
    | grep -o "[^:]*$" \
    | cut -d ' ' -f2 \
    | head -1)"
LBA="$(dumpet -i Win*.iso \
    | grep "LBA" \
    | tail -n 1 \
    | awk '{printf $3}')"
CDROM_SECTOR_SIZE=2048
FLOPPY_SECTOR_SIZE=512

dd \
    if=`printf Win*.iso` \
    bs=${CDROM_SECTOR_SIZE} \
    skip=${LBA} \
    count=1 \
    > win.dmp
FLOPPY_SECTORS="$(file win.dmp \
    | sed -e "s/.*sectors\ //" \
    | awk '{printf $1}')"

CDROM_SECTORS=`expr $FLOPPY_SECTOR_SIZE \
    \* $FLOPPY_SECTORS \
    / $CDROM_SECTOR_SIZE`

dd \
    if=`printf Win*.iso` \
    bs=${CDROM_SECTOR_SIZE} \
    skip=${LBA} \
    count=${CDROM_SECTORS} \
    of=win_efi_boot.img

# install mkisofs
# recreate iso
cd ${EXTRACT}
mkisofs \
    -allow-limited-size \
    -no-emul-boot \
    -b ${BOOT} \
    -boot-load-seg ${LOAD_SEG} \
    -boot-load-size ${LOAD_SIZE} \
    -eltorito-alt-boot \
    -b ${EFI} \
    -eltorito-platform efi \
    -iso-level 2 \
    -boot-info-table \
    -udf \
    -J -l -joliet-long \
    -D \
    -N \
    -relaxed-filenames \
    -V "WINSP" \
    -o winsp.iso \
    .

mv winsp.iso ..

# clean up bootsect and iso extract
cd ..
rm win.dmp
#rm -rf bootsect/
sudo rm -rf ${EXTRACT}
