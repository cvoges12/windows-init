#!/bin/sh

# install qemu for qemu-system-x86_64
# install spice-client-gtk for spicy
# install spice-vdagent for spice and vdagent
# libvirt-daemon for /shrug
SPICE_PORT=5930
CHARDEV_ID='spicechannel0'
DRIVE=$argv
ISO='Win*.iso'
sudo qemu-system-x86_64 \
    -enable-kvm \
    -daemonize \
    -cpu host \
    -smp 4 \
    -net nic \
    -net user,hostname=windowsvm \
    -m 8G \
    -boot d \
    -cdrom ${ISO} \
    -hda ${DRIVE} \
    -vga qxl \
    -device virtio-serial-pci \
    -spice port=${SPICE_PORT},disable-ticketing=on \
    -device virtserialport,chardev=${CHARDEV_ID},name=com.redhat.spice.0 \
    -chardev spicevmc,id=${CHARDEV_ID},name=vdagent
    #-drive file=${DRIVE},format=raw,cache=none,if=virtio \

exec spicy --title Windows 127.0.0.1 -p ${SPICE_PORT}

# clean up iso
rm ${ISO}
